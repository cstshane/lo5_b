<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>LO5-Procedural</title>
    </head>
    <body>
        <h1>LO5-Procedural</h1>

<?php
//Open a connection to MySQL (call mysqli_connect)
$db = @mysqli_connect( "kelcstu06", "INS214", "RASSLER", "INS214" );

//If the database connection failed (mysqli_connect returns false)
if ( !$db )
{
//Then
//	Perform processing as required to handle the error
    echo "<p>Failed to connect to database: " . mysqli_connect_error() .
            "</p>\n";
    exit();
}

//Else
//	Go ahead with normal processing on your web page
//	Close the database connection (call mysqli_close)
else
{
    echo "<p>Successfully connected to the database!</p>\n";
}

// Query the database
$qryResults = mysqli_query( $db, "SELECT firstName, lastName FROM Instructor" );

// If the query succeeded
if ( $qryResults )
{
    // Determine the number of rows returned and display it
    $numRows = mysqli_num_rows( $qryResults );
    echo "<p>Fetched $numRows rows from the Instructor table</p>\n";
    
    // We're done with the results -- free them
    mysqli_free_result( $qryResults );
}
else
{
    // Indicate that we failed to retrieve the results
    echo "<p>Failed to fetch from the Instructor table</p>\n";
}

mysqli_close( $db );

?>
    </body>
</html>
